extends Sprite2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# followMouse
	pass


func getResolution():
	var size = get_viewport().get_size()
	return Vector2(size)

# get zoom factor of the viewport stretching
func getResolutionFactor():
	return getResolution() / get_viewport_rect().size

# combine the camera zoom and viewport stretching
func getZoomFactor():
	return %Camera2D.zoom * getResolutionFactor()

func _input(event):
	if event is InputEventMouseMotion:
		# get mouseposition as global_position
		var mousePosition = (event.global_position-getResolution()/2) / getZoomFactor() + %Camera2D.get_screen_center_position() 
		# point from arrow to mouse
		var direction = Vector2(mousePosition - global_position).normalized()
		# calculate vector into radians
		rotation = atan2(direction.y, direction.x)
		get_parent().get_node("DebugLabel").text = str(snappedf(rotation,0.0001))
	elif event is InputEventMouseButton:
		# get relative mouse Input from screen
		var relativeMousePos = (event.global_position-getResolution()/2) / getZoomFactor()
		var cameraPos = %Camera2D.get_screen_center_position()
		var mousePos = cameraPos+relativeMousePos
		#if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			#spawnNode(%Player, mousePos)
		if event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
			var line = drawLine(mousePos,global_position)
			spawnNode(get_parent().get_node("DebugLabel"), mousePos, false, line)
	elif event.is_action_pressed("space"):
		if %LineList.get_child_count() > 0:
			if event.is_action_pressed("shift"):
				%LineList.get_child(0).queue_free()
			else:
				%LineList.get_child(-1).queue_free()

func spawnNode(node, givenPosition, text=false, parentNode=get_parent()):
	var new_node = node.duplicate()
	if text:
		new_node.text = text
	new_node.global_position = givenPosition
	new_node.visible = true
	parentNode.add_child(new_node)


func changeZoom(val):
	%Camera2D.zoom *= val
	if %Camera2D.zoom.x < 0.5:
		%Camera2D.zoom = Vector2(0.5,0.5)
	elif %Camera2D.zoom.x > 5:
		%Camera2D.zoom = Vector2(5,5)


func drawLine(vec, vec2, thiccness=1):
	var line = Line2D.new()
	line.add_point(vec)
	line.add_point(vec2)
	line.width = thiccness
	%LineList.add_child(line)
	return line



