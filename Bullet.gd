extends RigidBody2D


# Called when the node enters the scene tree for the first time.
func _ready():
	# linear_velocity = Vector2(-10,0)
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (constant_force.is_equal_approx(Vector2(0,0))):
		rotation = atan2(linear_velocity.y, linear_velocity.x)
	else:
		rotation = atan2(constant_force.y, constant_force.x)


func dieOff():
	$hitTimer.start()

func _on_timer_timeout():
	queue_free()
