extends Node2D

var suckers = Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	rotate(45 * delta)
	for suck in suckers:
		suck(suck)
	

func suck(body):
	# get normalized direction to Portal
	var direction = position - body.position
	direction = direction.normalized()
	# get strength
	var strength = 1/position.distance_to(body.position)
	strength = clamp(strength, 0.001,0.9) * 100
	# add to body the force
	body.outerForce = direction * strength * 10
	

func _on_area_2d_body_entered(body):
	suckers.append(body)



func _on_area_2d_body_exited(body):
	body.outerForce = Vector2(0,0)
	suckers.erase(body)
