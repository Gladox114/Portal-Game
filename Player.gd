extends CharacterBody2D


const SPEED = 25.0
const JUMP_VELOCITY = -400.0
var outerForce = Vector2(0,0)

var bullet_scene = load("res://bullet.tscn")



var portalHits = 0
var bodyHits = 0

func _physics_process(delta):
	playerMovement(delta)
	%Camera2D.global_position = global_position
	for index in get_slide_collision_count():
		var collision = get_slide_collision(index)
		print(collision)
		var colider = collision.get_collider()
		colider.apply_central_impulse(velocity * delta)
		colider.constant_force = Vector2(0,0)
		colider.linear_damp = 1.5
		colider.dieOff()
	# var bodies = $PortalFrame_rigid.get_colliding_bodies()
	# for index in bodies.length():
		# $PortalFrame_rigid.

func playerMovement(delta):
	var direction = getKeyboardDirection()
	if (direction.is_equal_approx(Vector2(0,0))):
		velocity -= velocity * delta * 10
	else:
		velocity += direction * SPEED * delta * 20 + outerForce
		velocity = velocity.limit_length(100)
	move_and_slide()

# input data
##############

# get position
# needs zoomFactor
func getMousePos(event):
	var mousePosition = (event.global_position-getResolution()/2) / getZoomFactor() + %Camera2D.get_screen_center_position() 
	return mousePosition

# get player direction
func getKeyboardDirection():
	# get movement input
	var direction = Vector2(
		Input.get_axis("player_left", "player_right"),
		Input.get_axis("player_up", "player_down")
	)
	# So the Player can't walk faster in Corner Directions (Up+Right for example)
	direction = direction.normalized()
	return direction

# Display calculations mostly for Mouse
# #####################################
# get display res
func getResolution():
	var size = get_viewport().get_size()
	return Vector2(size)

# get zoom factor of the viewport stretching
func getResolutionFactor():
	return getResolution() / get_viewport_rect().size

# combine the camera zoom and viewport stretching
func getZoomFactor():
	return %Camera2D.zoom * getResolutionFactor()

	
# vector calcualtions for mouse
# #############################
# get direction from source to target
func getDirection(source, target):
	return Vector2(target - source).normalized()


# process mouse and keyboard input
# changes rotation live
# changes zoom live
func _input(event):
	if event is InputEventMouseMotion:
		# get mouseposition as global_position
		#var mousePosition = (event.global_position-getResolution()/2) / getZoomFactor() + %Camera2D.get_screen_center_position() 
		# point from arrow to mouse
		#var direction = Vector2(mousePosition - global_position).normalized()
		# calculate vector into radians
		var direction = getDirection(global_position, getMousePos(event));
		rotation = atan2(direction.y, direction.x)
		#print("movement: "+str(event.global_position))
	elif event is InputEventMouseButton:
		# get relative mouse Input from screen
		#var relativeMousePos = (event.global_position-getResolution()/2) / getZoomFactor()
		#var cameraPos = %Camera2D.get_screen_center_position()
		#var mousePos = cameraPos+relativeMousePos
		if event.button_index == MOUSE_BUTTON_WHEEL_UP and event.pressed:
			changeZoom(1.1)
		elif event.button_index == MOUSE_BUTTON_WHEEL_DOWN and event.pressed:
			changeZoom(0.9)
		elif event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			spawnBullet(event)
			


func changeZoom(val):
	%Camera2D.zoom *= val
	if %Camera2D.zoom.x < 0.5:
		%Camera2D.zoom = Vector2(0.5,0.5)
	elif %Camera2D.zoom.x > 5:
		%Camera2D.zoom = Vector2(5,5)

func spawnBullet(event):
	var bullet = bullet_scene.instantiate()
	bullet.global_position = getMousePos(event)
	print("click: "+str(event.global_position))
	%World.add_child(bullet)
	bullet.constant_force = getDirection(bullet.global_position, global_position)
	
func addPortalHits():
	portalHits += 1
	%PortalHits_Label.text = "PortalHits: " + str(portalHits)
	
func addBodyHits():
	bodyHits += 1
	%BodyHits_Label.text = "BodyHits: " + str(bodyHits)

func _on_portal_body_entered(body):
	print("Player detected:"+str(body))
	addPortalHits()
	body.queue_free()

func _on_body_body_entered(body):
	print("Body detected:"+str(body))
	addBodyHits()
	body.queue_free()

